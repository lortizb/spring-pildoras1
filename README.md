# Learning Spring

This project is to learn about spring and how to use it. I've followed the video tutorials of the *PildorasInformaticas* channel on Youtube.


**The project started from the video number 12**. I'll try to push all the code in this repo.


* **Youtube channel**: https://www.youtube.com/channel/UCdulIs-x_xrRd1ezwJZR9ww
* **Spring playlist**: https://www.youtube.com/watch?v=ur38aDTMNuc