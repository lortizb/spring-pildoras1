package com.grupompr.ioc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoDemoSingletonProtoype {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext2.xml");
		
		// Petici�n de beans al contenedor
		
		SecretarioEmpleado maria = context.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);
		
		SecretarioEmpleado pedro = context.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);
		
		System.out.println(maria);
		System.out.println(pedro);
		
		if(maria == pedro) {
			System.out.println("Apunta al mismo objeto");
		}else {
			System.out.println("No apuntan al mismo objeto");
		}
		
		context.close();

	}

}
