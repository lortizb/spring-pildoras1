package com.grupompr.ioc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoEmpleados {

	public static void main(String[] args) {

		//Creaci�n de objetos de tipo empleados
		
		//Empleados empleado1= new SecretarioEmpleado();
		
		// uso de los objetos
		//System.out.println(empleado1.getTareas());

		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		/*Empleados leodrumers=contexto.getBean("miEmpleado",Empleados.class);
		System.out.println(leodrumers.getTareas());
		System.out.println(leodrumers.getInforme());*/
		
		SecretarioEmpleado leodrumers=contexto.getBean("miSecetarioEmpleado",SecretarioEmpleado.class);
		System.out.println(leodrumers.getTareas());
		System.out.println(leodrumers.getInforme());
		System.out.println("Email: " + leodrumers.getEmail());
		System.out.println("Empresa: " + leodrumers.getNombreEmpresa());
		
		
		contexto.close();
		
		
	}

}
